# xkcd Nav

Enhance navigation experience on xkcd.com.

## Overview

xkcd Nav is a Chrome extension that provide features to navigate the xkcd.com
webcomic site. xkcd Nav will automatically add the caption and posting date
below the comic. The caption is the comic's alt-text, or the text you see when
you mouse over the comic. The posting date, or publication date, is the date
you see when you mouse over a comic link in the archives. By pressing certain
keys, xkcd Nav will navigate you to the next comic, the previous comic, a
random comic, and more. Read xkcd without leaving your keyboard.

## Navigation Keys

| Key | Action           |
|:---:|:-----------------|
|  H  | previous comic   |
|  L  | next comic       |
|  J  | scroll page down |
|  K  | scroll page up   |
|  [  | oldest comic     |
|  ]  | newest comic     |
|  \  | random comic     |

These key mappings are vim-like, and allow you to navigate comics without
leaving your home row most of the time.

## Install

Manually use Developer Mode in Chrome to install the extension:

1. Download this repo.
2. In Chrome, go to [chrome://extensions](chrome://extensions).
3. Check the `Developer mode` box.
4. Select `Load unpacked extension`.
5. Select the repo you downloaded.

## Uninstall

1. In Chrome, go to [chrome://extensions](chrome://extensions).
2. Find `xkcd Nav`.
3. Click the trash icon.
4. Confirm by selecting `Remove`.

## License

MIT License
