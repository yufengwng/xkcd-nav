// Build xkcdNav namespace.
var xkcdNav = xkcdNav || {};

xkcdNav.ajaxGet = function(url, callback) {
  var request = new XMLHttpRequest();
  request.onreadystatechange = function() {
    if (request.readyState == 4 && request.status == 200) {
      // Parse response from the request object.
      var metadata = JSON.parse(request.responseText);
      callback(metadata);
    }
  };
  request.open('GET', url, true);
  request.send();
};

xkcdNav.insertAfter = function(newNode, targetNode) {
  targetNode.parentNode.insertBefore(newNode, targetNode.nextSibling);
};

xkcdNav.createDateNode = function(metadata) {
  var d = new Date(metadata.year, metadata.month - 1, metadata.day);
  var postDate = document.createElement('p');
  postDate.setAttribute('id', 'xkcdNav-date');
  postDate.innerText = 'Posted: ' + d.toDateString();
  return postDate;
};

xkcdNav.createCaptionNode = function(metadata) {
  var caption = document.createElement('p');
  caption.setAttribute('id', 'xkcdNav-caption');
  caption.innerText = metadata.alt;
  return caption;
};

xkcdNav.parseComicNav = function(index) {
  var navClass = 'comicNav';
  var navBar = document.getElementsByClassName(navClass)[0];
  var navButton = navBar.children[index];
  var navLink = navButton.children[0];
  return navLink.href;
};

xkcdNav.redirectRandomComic = function() {
  window.location.href = xkcdNav.parseComicNav(2);
};

xkcdNav.redirectNewerComic = function() {
  window.location.href = xkcdNav.parseComicNav(3);
};

xkcdNav.redirectOlderComic = function() {
  window.location.href = xkcdNav.parseComicNav(1);
};

xkcdNav.redirectFirstComic = function() {
  window.location.href = xkcdNav.parseComicNav(0);
};

xkcdNav.redirectLastComic = function() {
  window.location.href = xkcdNav.parseComicNav(4);
};

xkcdNav.scrollDown = function(increment) {
  window.scrollBy(0, increment);
}

xkcdNav.scrollUp = function(increment) {
  window.scrollBy(0, increment * -1);
}

xkcdNav.keyCodes = {
  '[': 91,
  '\\': 92,
  ']': 93,
  'A': 97,
  'D': 100,
  'E': 101,
  'H': 104,
  'J': 106,
  'K': 107,
  'L': 108,
  'Q': 113,
  'R': 114,
}

xkcdNav.scrollStep = 40;

// End xkcdNav namespacing.

// Register key listeners.
window.addEventListener('keypress', function(e) {
  if (e.keyCode === xkcdNav.keyCodes['\\']) {
    xkcdNav.redirectRandomComic();
  } else if (e.keyCode === xkcdNav.keyCodes['L']) {
    xkcdNav.redirectNewerComic();
  } else if (e.keyCode === xkcdNav.keyCodes['H']) {
    xkcdNav.redirectOlderComic();
  } else if (e.keyCode === xkcdNav.keyCodes['[']) {
    xkcdNav.redirectFirstComic();
  } else if (e.keyCode === xkcdNav.keyCodes[']']) {
    xkcdNav.redirectLastComic();
  } else if (e.keyCode === xkcdNav.keyCodes['J']) {
    xkcdNav.scrollDown(xkcdNav.scrollStep);
  } else if (e.keyCode === xkcdNav.keyCodes['K']) {
    xkcdNav.scrollUp(xkcdNav.scrollStep);
  }
}, false);

// Request comic metadata and enhance DOM.
var endpoint = window.location.href + 'info.0.json';
xkcdNav.ajaxGet(endpoint, function(metadata) {
  // Setup nodes.
  var caption = xkcdNav.createCaptionNode(metadata);
  var postDate = xkcdNav.createDateNode(metadata);

  // Insert into DOM.
  var comic = document.getElementById('comic');
  xkcdNav.insertAfter(postDate, comic);
  xkcdNav.insertAfter(caption, comic);
});
